#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

const double PI = 3.141592;

class Vector2i
{
public:
    int x;
    int y;

    Vector2i()
    {
        x = 0;
        y = 0;
    }
    Vector2i(int p_x, int p_y)
    {
        x = p_x;
        y = p_y;
    }

    void add_x(int delta_x) { x += delta_x; }
    void add_y(int delta_y) { y += delta_y; }

    // pour obtenir la norme
    float get_norme() { return pow(x * x + y * y, 0.5f); }

    //
    int calculate_angle()
    {
        if (x >= 0)
        {
            return atan(y / x);
        }
        else
        {
            return PI - atan(-y / x);
        }
    }

    Vector2i operator+(Vector2i const &vector2)
    {
        Vector2i new_vect(x + vector2.x, y + vector2.y);
        return new_vect;
    }
    Vector2i operator-(Vector2i const &vector2)
    {
        Vector2i new_vect(x - vector2.x, y - vector2.y);
        return new_vect;
    }
    Vector2i operator*(int const &coef)
    {
        Vector2i new_vect(x, y);
        new_vect.x *= coef;
        new_vect.y *= coef;
        return new_vect;
    }
};
std::ostream &operator<<(std::ostream &os, const Vector2i &v)
{
    os << '{' << v.x << ',' << v.y << '}';
    return os;
}

const Vector2i basePositions[2] = {Vector2i(0, 0), Vector2i(16000, 9000)};

float get_dist(Vector2i v1, Vector2i v2) { return (v1 - v2).get_norme(); }

enum ENTITY_TYPE // -1 -> ghost, 0 -> buster0, 1 -> buster1
{
    GHOST_TYPE = -1,
    BUSTER_0 = 0,
    BUSTER_1 = 1
};

enum ENTITY_ROLE // -1 -> ghost, 0 -> hunter, 1 -> ghostcatcher, 2 -> support
{
    GHOST_ROLE = -1,
    HUNTER = 0,
    GHOST_CATCHER = 1,
    SUPPORT = 2
};

class Entity
{
protected:
    int id;
    Vector2i position;
    ENTITY_TYPE entityType;
    ENTITY_ROLE entityRole;
    int state;
    int value;
    int lastSeen; //when the entity was last seen in round number

public:
    Entity() : id(-1), position(), entityType(GHOST_TYPE), entityRole(GHOST_ROLE), state(0), value(0), lastSeen(-1) {}
    Entity(int id, Vector2i position, ENTITY_TYPE entityType, ENTITY_ROLE entityRole, int state, int value, int turnNb = 0) : lastSeen(turnNb)
    {
        this->id = id;
        this->position = position;
        this->entityType = entityType;
        this->entityRole = entityRole;
        this->value = value;
    }

    friend std::ostream &operator<<(std::ostream &os, const Entity &e)
    {
        os << "Entity team:" << e.entityType << " role:" << e.entityRole << " id:" << e.id;
        return os;
    }

    // GETTER & SETTER
    int get_id() const { return this->id; }
    Vector2i get_position() const { return this->position; }
    ENTITY_TYPE get_entityType() const { return this->entityType; }
    ENTITY_ROLE get_entityRole() const { return this->entityRole; }
    int get_state() const { return this->state; }
    int get_value() const { return this->value; }
    int get_lastSeen() const { return this->lastSeen; }

    void set_state(const int _state) { this->state = _state; }
    void set_value(const int _value) { this->value = _value; }
    void set_lastSeen(const int _turnNb) { this->lastSeen = _turnNb; }

    //Functions
    void update(int entityId, Vector2i pos, int state, int value, int turnNb)
    {
        { //Debug
            std::cerr << "DEBUG INIT TURN. Updating entity. team:" << entityType << " role:" << entityRole << " id:" << id << std::endl;
            if (this->id != -1 && this->id != entityId)
                std::cerr << "WARNING Entity::update(). Id does not match." << std::endl;
        }

        this->id = entityId;
        this->position = pos;
        this->state = state;
        this->value = value;
        this->lastSeen = turnNb;
    }

    void debug_entity() const
    {
        std::cerr << "Entity type:" << this->entityType << " role:" << this->entityRole << " id:" << this->id << " pos:" << this->position << std::endl;
    }
    void display_action() const
    {
        std::cout << "MOVE 8000 4500" << std::endl;
    }
};

class Ghost : public Entity
{
private:
public:
    Ghost() {}
    Ghost(int id, Vector2i position, int state, int value) : Entity(id, position, GHOST_TYPE, GHOST_ROLE, state, value) {}
};

class Ghosts
{
private:
    std::vector<Ghost> ghosts;

public:
    Ghosts() { ghosts.resize(0); }

    friend std::ostream &operator<<(std::ostream &os, const Ghosts &g)
    {
        os << "Ghosts : " << std::endl;
        for (auto i = g.ghosts.begin(); i < g.ghosts.end(); i++)
        {
            os << *i << std::endl;
        }
        os << "End ghosts" << std::endl;

        return os;
    }

    void init(int ghostNb) { ghosts.resize(ghostNb); }
    void update_ghost(int entityId, Vector2i pos, int state, int value, int turnNb)
    {
        Ghost *correspondingGhost = get_ghost_corresponding_to_id(entityId);
        if (correspondingGhost == nullptr)
            correspondingGhost = get_first_unknown_ghost();
        if (correspondingGhost == nullptr)
            std::cerr << "ERROR Ghosts::update_ghost(). no corresponding ghost" << std::endl;

        correspondingGhost->update(entityId, pos, state, value, turnNb);
    }

    Ghost *get_ghost_corresponding_to_id(int id)
    {
        for (auto i = ghosts.begin(); i < ghosts.end(); i++)
        {
            if (i->get_id() == id)
                return &(*i);
        }

        return nullptr;
    }
    Ghost *get_first_unknown_ghost()
    {
        for (auto i = ghosts.begin(); i < ghosts.end(); i++)
        {
            if (i->get_id() == -1)
                return &(*i);
        }

        return nullptr;
    }
};

class Buster : public Entity
{
private:
public:
    Buster() {}
    Buster(int id, Vector2i position, ENTITY_TYPE entityType, ENTITY_ROLE entityRole, int state, int value, int turnNb = 0) : Entity(id, position, entityType, entityRole, state, value, turnNb) {}
};

class Hunter : public Buster
{
private:
public:
    Hunter() { this->entityRole = HUNTER; }
    Hunter(int id, Vector2i position, ENTITY_TYPE entityType, int state, int value, int turnNb = 0) : Buster(id, position, entityType, HUNTER, state, value, turnNb)
    {
        std::cerr << "DEBUG INIT. Creating Hunter. team:" << entityType << ". id:" << id << std::endl;
    }
    Hunter(Buster buster) : Buster(buster.get_id(), buster.get_position(), buster.get_entityType(), HUNTER, buster.get_state(), buster.get_value()) {}
};

class GhostCatcher : public Buster
{
private:
public:
    GhostCatcher() { this->entityRole = GHOST_CATCHER; }
    GhostCatcher(int id, Vector2i position, ENTITY_TYPE entityType, int state, int value, int turnNb = 0) : Buster(id, position, entityType, GHOST_CATCHER, state, value, turnNb)
    {
        std::cerr << "DEBUG INIT. Creating GhostCatcher. team:" << entityType << ". id:" << id << std::endl;
    }
    GhostCatcher(Buster buster) : Buster(buster.get_id(), buster.get_position(), buster.get_entityType(), GHOST_CATCHER, buster.get_state(), buster.get_value()) {}
};

class Support : public Buster
{
private:
public:
    Support() { this->entityRole = SUPPORT; }
    Support(int id, Vector2i position, ENTITY_TYPE entityType, int state, int value, int turnNb = 0) : Buster(id, position, entityType, SUPPORT, state, value, turnNb)
    {
        std::cerr << "DEBUG INIT. Creating Support. team:" << entityType << ". id:" << id << std::endl;
    }
    Support(Buster buster) : Buster(buster.get_id(), buster.get_position(), buster.get_entityType(), SUPPORT, buster.get_state(), buster.get_value()) {}
};

class Player
{
private:
    int score;
    int teamId;
    bool isAllied;

    Hunter hunter;
    GhostCatcher ghostCatcher;
    Support support;

public:
    Player() : score(0) {}

    void init(int teamId, bool isAllied)
    {
        std::cerr << "DEBUG INIT. Creating Player" << teamId << ". is Allied:" << isAllied << std::endl;

        this->teamId = teamId;
        this->isAllied = isAllied;

        this->hunter = Hunter(-1, basePositions[teamId], (ENTITY_TYPE)teamId, -1, -1, 0);
        this->ghostCatcher = GhostCatcher(-1, basePositions[teamId], (ENTITY_TYPE)teamId, -1, -1, 0);
        this->support = Support(-1, basePositions[teamId], (ENTITY_TYPE)teamId, -1, -1, 0);
    }
    void update_entity(int entityId, Vector2i pos, ENTITY_ROLE entityRole, int state, int value, int turnNb)
    {
        if (entityRole == HUNTER)
        {
            this->hunter.update(entityId, pos, state, value, turnNb);
        }
        else if (entityRole == GHOST_CATCHER)
        {
            this->ghostCatcher.update(entityId, pos, state, value, turnNb);
        }
        else if (entityRole == SUPPORT)
        {
            this->support.update(entityId, pos, state, value, turnNb);
        }
        else
        {
            std::cerr << "ERROR Player::update_entity(). unknown entity role:" << entityRole << std::endl;
        }
    }
};

class Game
{
private:
    int myTeamId;
    int turnNb;
    int ghostCount;

    Player myPlayer;
    Player ennemyPlayer;

    Ghosts ghosts;

    // private methods
    void update_entity(int entityId, Vector2i pos, ENTITY_TYPE entityType, ENTITY_ROLE entityRole, int state, int value, int turnNb)
    {
        if (entityType == GHOST_TYPE)
        {
            ghosts.update_ghost(entityId, pos, state, value, turnNb);
        }
        else if (entityType == myTeamId)
        {
            myPlayer.update_entity(entityId, pos, entityRole, state, value, turnNb);
        }
        else if (entityType == (myTeamId + 1) % 2)
        {
            ennemyPlayer.update_entity(entityId, pos, entityRole, state, value, turnNb);
        }
        else
        {
            std::cerr << "ERROR Game::update_entity(). unknown entity type:" << entityType << std::endl;
        }
    }

public:
    Game()
    {
        int bustersPerPlayer; // the amount of busters you control
        std::cin >> bustersPerPlayer;
        std::cin.ignore();
        std::cin >> ghostCount;
        std::cin.ignore();
        std::cin >> myTeamId;
        std::cin.ignore();

        turnNb = 0;

        myPlayer.init(myTeamId, true);
        ennemyPlayer.init((myTeamId + 1) % 2, false);

        ghosts.init(ghostCount);
    }

    int get_myTeamId() const { return this->myTeamId; }
    int get_roundNB() const { return this->turnNb; }
    Player &get_myTeam() { return this->myPlayer; }
    Player &get_enemyTeam() { return this->ennemyPlayer; }

    void play()
    {
        while (turnNb < 250)
        {
            this->init_new_turn();
            this->end_turn();
            this->display_answer();
        }
    }

    void init_new_turn()
    {
        turnNb++;

        int entities; // the number of busters and ghosts visible to you
        std::cin >> entities;
        std::cin.ignore();
        //std::cerr << "New Turn" << std::endl;
        //std::cerr << "nb of entities:" << entities << std::endl;

        int entityId; // buster id or ghost id
        int x;
        int y;          // position of this buster / ghost
        int entityType; // the team id if it is a buster, -1 if it is a ghost.
        int entityRole; // -1 for ghosts, 0 for the HUNTER, 1 for the GHOST CATCHER and 2 for the SUPPORT
        int state;      // For busters: 0=idle, 1=carrying a ghost. For ghosts: remaining stamina points.
        int value;      // For busters: Ghost id being carried/busted or number of turns left when stunned. For ghosts: number of busters attempting to trap this ghost.
        for (int i = 0; i < entities; i++)
        {
            std::cin >> entityId >> x >> y >> entityType >> entityRole >> state >> value;
            std::cin.ignore();
            //std::cerr << "id:" << entityId << " pos:" << x << "," << y << " type:" << entityType << " role:" << entityRole << " state:" << state << " value:" << value << std::endl;
            this->update_entity(entityId, Vector2i(x, y), (ENTITY_TYPE)entityType, (ENTITY_ROLE)entityRole, state, value, turnNb);
        }
    }
    void end_turn()
    {
        std::cerr << ghosts; // DEBUG : displays ghosts vector
    }
    void display_answer()
    {
        // First the HUNTER : MOVE x y | BUST id
        // Second the GHOST CATCHER: MOVE x y | TRAP id | RELEASE
        // Third the SUPPORT: MOVE x y | STUN id | RADAR
        std::cout << "MOVE 8000 4500" << std::endl;
        std::cout << "MOVE 8000 4500" << std::endl;
        std::cout << "MOVE 8000 4500" << std::endl;
    }
};

int main()
{
    Game game;

    game.play();
}